$(function(){
  var arkHtml = $('html');
  var arkBody = $('body');
  var arkWindow = $(window);
  var arkMenuBtn = $('.ark-menu-btn');
  var arkMobileMenu = $('.ark-header-items');
  var arkTabHeaderItem = $('.ark-tab-header-item');
  var arkTabBodyItem = $('.ark-tab-item');
  var arkHeaderMenuLink = $('header nav ul li');
  var arkDropdownItem = $('.ark-dropdown li');
  var arkDropdownSelectedVal = $('.ark-selected-val');
  var arkScrollTopBtn = $('.ark-scroll-top');

  /*Toogle mobile menu*/
  arkMenuBtn.on('click', function(){
    arkMobileMenu.toggleClass('open');
    $(this).toggleClass('active');
  });
  /*------------------*/

  /*Toggle header menu*/
  arkHeaderMenuLink.on('click', function(){
    arkHeaderMenuLink.removeClass('open');
    $(this).addClass('open');
    return false;
  });

  arkDropdownItem.on('click', function(){
    var arkCurrentVal = $(this).parents('.ark-menu-item').find(arkDropdownSelectedVal);
    var arkSelectedval = $(this).text();

    if (arkCurrentVal.length) {
      arkCurrentVal.html(arkSelectedval);
    }
  });

  arkHtml.click(function () {
    arkHeaderMenuLink.removeClass('open');
  })
  /*------------------*/

  /*Tab---------------*/
  arkTabHeaderItem.on('click', function(){
    var currentTab = $(this).attr('data-tab-link');

    arkTabHeaderItem.removeClass('active');
    $(this).addClass('active');
    arkTabBodyItem.removeClass('active');
    $(currentTab).addClass('active');
  });
  /*------------------*/

  /*Scroll to top----------------------------*/
  arkWindow.scroll(function(){
    var arkTopOffset = 500;
    var arkScrollTime = 500;

    if(arkBody.scrollTop() > arkTopOffset ) {
      arkScrollTopBtn.fadeIn(arkScrollTime);
    }
    else{
      arkScrollTopBtn.fadeOut(arkScrollTime);
    }
  });

  arkScrollTopBtn.on('click', function(event){
      event.preventDefault();
      arkBody.animate({
        scrollTop: 0 ,
      }, 1000
    );
  });

})();
